# Calibration ifo configuration: L1

This repo holds calibration configuration files for the Livingston
observatory "L1" detector.

The primary file is the
[pydarm](https://git.ligo.org/Calibration/pydarm) calibration model
configuration .ini file for L1, starting after O3.

